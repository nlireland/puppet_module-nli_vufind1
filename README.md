# README #

This repository allows us to set up an instance of vufind

## Set-up for Vufind Instance##

Add the following 2 blocks of code to the node definition:

```
class { 'nli_vufind1': 
  vufind_database     => 'application_name',
  vufind_db_password  => 'password',  
  vufind_git_repo     => 'https://bitbucket.org/USER_ACCOUNT/application_name.git',
  vufind_url_path     => 'IP_ADDRESS or ROUTER' 
  remote_database     => true,
  vufind_db_host      => 'IP_ADDRESS OF REMOTE DATABASE HOST',
  require             => Netrc::Foruser["netrc_user"],
}
```
* remote_database: only set this to 'true' if you wish to use a remote database - otherwise vufind will install the database locally - leave variable out if NOT using remote_database
* vufind_db_host: only needed if we are using a remote db host - leave variable out if NOT using remote_database


```
netrc::foruser{"netrc_user":    
  user => 'puppetadmin', 
  machine_user_password_triples => [
    ['bitbucket.org', hiera('bitbucket_login'), hiera('bitbucket_password')]
  ],
}
```

#### Apache Vhost

The Apache vhost must be set to the server url address or the server ip address

```
  apache::vhost{ $vufind_url_server: }

```
The apache vhost will create a file in the conf.d folder structure ... and will be named 10-$vufind_url_server.conf .... $vufind_url_server will default to the IP if not set

*It is important the vhost is named for without (http://) an example: catalogue.nli.ie*