class nli_vufind1::params{
  $vufind_git_repo    = undef
  $vufind_code_path   = "/usr/local/vufind"
  $pear_path          = "/usr/share/pear"
  $use_private_repo   = true
  $git_branch         = "master"
  $vufind_database    = undef
  $vufind_db_user     = "vufind"
  $vufind_db_host     = "localhost"
  $vufind_db_password = undef
  $vufind_url_path    = ''
  $vufind_url_server  = $ipaddress
  $solr_java_opts     = undef
  $enable_mod_proxy   = true
  $remote_database    = false
  $fileserver_mount   = '/path/to/puppet/fileserver'
  $install_oci_driver = false
  $enable_apc         = false
}