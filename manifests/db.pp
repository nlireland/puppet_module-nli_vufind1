define nli_vufind1::db (
  $vufind_db_user       = "vufind",
  $vufind_app_host      = undef,
  $vufind_db_password   = undef,
  $vufind_code_path     = undef,
) {
  $vufind_database      = $title

  if $vufind_app_host == undef {
    fail('The IP address or hostname of the VuFind application server must be passed as a param')
  } 

  if $vufind_db_password == undef {
    fail('The vufind password must be defined with the variable: vufind_db_password')
  }

  #### MySQL

  include 'mysql::server'

  include wget

  wget::fetch { "download-vufind-$vufind_database-mysql-file":
    source      => 'http://sourceforge.net/p/vufind/svn/HEAD/tree/trunk/mysql.sql?format=raw',
    destination => '/tmp/mysql.sql',
    timeout     => 0,
    verbose     => false,
  }

  mysql::db { $vufind_database:
    user           => $vufind_db_user,
    password       => $vufind_db_password,
    host           => $vufind_app_host,
    grant          => ['SELECT', 'INSERT', 'UPDATE', 'DELETE'],
    sql            => '/tmp/mysql.sql',
    import_timeout => 900,
    require        => Wget::Fetch["download-vufind-$vufind_database-mysql-file"],
  }
}
