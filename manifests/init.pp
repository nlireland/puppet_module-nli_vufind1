class nli_vufind1 (
  $vufind_git_repo    = $nli_vufind1::params::vufind_git_repo,
  $vufind_code_path   = $nli_vufind1::params::vufind_code_path,
  $pear_path          = $nli_vufind1::params::pear_path,
  $use_private_repo   = $nli_vufind1::params::use_private_repo,
  $git_branch         = $nli_vufind1::params::git_branch,
  $vufind_database    = $nli_vufind1::params::vufind_database,
  $vufind_db_user     = $nli_vufind1::params::vufind_db_user,
  $vufind_db_host     = $nli_vufind1::params::vufind_db_host,
  $vufind_db_password = $nli_vufind1::params::vufind_db_password,
  $vufind_url_path    = $nli_vufind1::params::vufind_url_path,
  $vufind_url_server  = $nli_vufind1::params::vufind_url_server,
  $solr_java_opts     = $nli_vufind1::params::solr_java_opts,
  $enable_mod_proxy   = $nli_vufind1::params::enable_mod_proxy,
  $remote_database    = $nli_vufind1::params::remote_database,
  $install_oci_driver = $nli_vufind1::params::install_oci_driver,
  $enable_apc         = $nli_vufind1::params::enable_apc 
  ) inherits nli_vufind1::params {

  include git


  file { $vufind_code_path:
    ensure  => 'directory',
    recurse => true,
    mode    => "0664",
    owner   => 'puppetadmin',
    group   => 'puppetadmin',
  }

  if $vufind_git_repo == undef {
    fail('The vufind git repo must be defined with the variable: vufind_git_repo')
  } else {
    vcsrepo { "$vufind_code_path":
      ensure   => latest,
      provider => git,
      source   => $vufind_git_repo,
      revision => $git_branch,
      owner    => 'puppetadmin',
      group    => 'puppetadmin',
      user     => 'puppetadmin',
      require  => [Package["git"], File["$vufind_code_path"]],
    }
  }

  if $remote_database == false {

    if $vufind_database == undef {
      fail('For localhost databases, you must provide the $vufind_database param')
    }

    nli_vufind1::db { $vufind_database:
      vufind_db_user     => $vufind_db_user,
      vufind_app_host    => $vufind_db_host,
      vufind_db_password => $vufind_db_password,
      vufind_code_path   => $vufind_code_path,
      require            => Vcsrepo["$vufind_code_path"],
    }
  }


  #### Modify Config.ini file with settings

  file { 'config.ini':
    path    => "$vufind_code_path/web/conf/config.ini",
    ensure  => 'present',
    source  => "$vufind_code_path/web/conf/config.ini.EXAMPLE",
    require => Vcsrepo["$vufind_code_path"],
  }


  ini_setting { '[Site] path':
    ensure  => present,
    section => 'Site',
    setting => 'path',
    value   => $vufind_url_path,
    path    => "$vufind_code_path/web/conf/config.ini",
    require => File['config.ini'],
  } ->
  ini_setting { '[Site] url':
    ensure  => present,
    section => 'Site',
    setting => 'url',
    value   => "http://$vufind_url_server",
    path    => "$vufind_code_path/web/conf/config.ini",
  } ->
  ini_setting { '[Database] database':
    ensure  => present,
    section => 'Database',
    setting => 'database',
    value   => "mysql://$vufind_db_user:$vufind_db_password@$vufind_db_host/$vufind_database",
    path    => "$vufind_code_path/web/conf/config.ini",
  } ->
  ini_setting { '[Database] schema_location':
    ensure  => present,
    section => 'Database',
    setting => 'schema_location',
    value   => "$vufind_code_path/web/conf",
    path    => "$vufind_code_path/web/conf/config.ini",
  } ->
  ini_setting { '[Database] class_location':
    ensure  => present,
    section => 'Database',
    setting => 'class_location',
    value   => "$vufind_code_path/web/services/MyResearch/lib",
    path    => "$vufind_code_path/web/conf/config.ini",
  } ->
  ini_setting { '[Index] local':
    ensure  => present,
    section => 'Index',
    setting => 'local',
    value   => "$vufind_code_path/solr",
    path    => "$vufind_code_path/web/conf/config.ini",
  } ->
  ini_setting { '[Index] url':
    ensure  => present,
    section => 'Index',
    setting => 'url',
    value   => 'http://localhost:8080/solr',
    path    => "$vufind_code_path/web/conf/config.ini",
  } ->
  ini_setting { '[Statistics] solr':
    ensure  => present,
    section => 'Statistics',
    setting => 'solr',
    value   => 'http://localhost:8080/solr',
    path    => "$vufind_code_path/web/conf/config.ini",
    notify  => Service["httpd"],
  }


  #### URL looking for vufind.ini so copy file and rename to the application name

  file { "$vufind_database.ini":
    path    => "$vufind_code_path/web/conf/$vufind_database.ini",
    ensure  => 'present',
    source  => "$vufind_code_path/web/conf/vufind.ini",
    require => Vcsrepo["$vufind_code_path"],
    notify  => Service["httpd"],
  }


  #### PHP Install
  
  $php_packages = ['php', 'php-devel', 'php-pear', 'php-ldap', 'php-mysql', 'php-gd', 'php-pspell', 'php-mbstring']

  # subcrition manager for enabling rhel 7 repo optional server for php-pspell, php-mbstring, php-devel
  package { $php_packages:
    ensure  => 'installed',
    install_options => ['--enablerepo=rhel-7-server-optional-rpms'],
  }
 

 #### PEAR Package Installation

  exec { "pear-install-DB":
    command => "/usr/bin/pear install --onlyreqdeps DB-1.8.2",
    creates => "$pear_path/DB",
    require => Package['php-pear'],
  } ->
  exec { "pear-install-DB_DataObject-1.11.3":
    command => "/usr/bin/pear install --onlyreqdeps DB_DataObject-1.11.3",
    creates => "$pear_path/DB/DataObject",
  } ->
  exec { "pear-install-Structures_DataGrid-beta":
    command => "/usr/bin/pear install --onlyreqdeps Structures_DataGrid-0.9.3",
    creates => "$pear_path/Structures/DataGrid.php",
  } ->
  exec { "pear-install-Structures_DataGrid_DataSource_DataObject-beta":
    command => "/usr/bin/pear install --onlyreqdeps Structures_DataGrid_DataSource_DataObject-0.2.2dev1",
    creates => "$pear_path/Structures/DataGrid/DataSource/DataObject.php",
  } ->
  exec { "pear-install-Structures_DataGrid_DataSource_Array-beta":
    command => "/usr/bin/pear install --onlyreqdeps Structures_DataGrid_DataSource_Array-0.2.0dev1",
    creates => "$pear_path/Structures/DataGrid/DataSource/Array.php",
  } ->
  exec { "pear-install-Structures_DataGrid_Renderer_Pager-beta":
    command => "/usr/bin/pear install --onlyreqdeps Structures_DataGrid_Renderer_Pager-0.1.3",
    creates => "$pear_path/Structures/DataGrid/Renderer/Pager.php",
  } ->
  exec { "pear-install-Structures_DataGrid_Renderer_HTMLTable-beta":
    command => "/usr/bin/pear install --onlyreqdeps Structures_DataGrid_Renderer_HTMLTable-0.1.6",
    creates => "$pear_path/Structures/DataGrid/Renderer/HTMLTable.php",
  } ->
  exec { "pear-install-HTTP_Client":
    command => "/usr/bin/pear install --onlyreqdeps HTTP_Client-1.2.1",
    creates => "$pear_path/HTTP/Client",
  } ->
  exec { "pear-install-HTTP_Request":
    command => "/usr/bin/pear install --onlyreqdeps HTTP_Request-1.4.4",
    creates => "$pear_path/HTTP/Request",
  } ->
  exec { "pear-install-Log":
    command => "/usr/bin/pear install --onlyreqdeps Log-1.12.9",
    creates => "$pear_path/Log",
  } ->
  exec { "pear-install-Mail":
    command => "/usr/bin/pear install --onlyreqdeps Mail-1.2.0",
    creates => "$pear_path/Mail",
  } ->
  exec { "pear-install-Mail_Mime":
    command => "/usr/bin/pear install --onlyreqdeps Mail_Mime-1.10.0",
    creates => "$pear_path/Mail/mime.php",
  } ->
  exec { "pear-install-Net_SMTP":
    command => "/usr/bin/pear install --onlyreqdeps Net_SMTP-1.7.1",
    creates => "$pear_path/Net/SMTP.php",
  } ->
  exec { "pear-install-Pager":
    command => "/usr/bin/pear install --onlyreqdeps Pager-2.4.9",
    creates => "$pear_path/Pager",
  } ->
  exec { "pear-install-XML_RSS":
    command => "/usr/bin/pear install --onlyreqdeps XML_RSS-1.0.2",
    creates => "$pear_path/XML/RSS.php",
  } ->
  exec { "pear-install-XML_Serializer-beta":
    command => "/usr/bin/pear install --onlyreqdeps XML_Serializer-0.20.2",
    creates => "$pear_path/XML/Serializer.php",
  } ->
  exec { "pear-install-Console_ProgressBar-beta":
    command => "/usr/bin/pear install --onlyreqdeps Console_ProgressBar-0.5.2beta",
    creates => "$pear_path/Console/ProgressBar.php",
  } ->
  exec { "pear-install-File_Marc-alpha":
    command => "/usr/bin/pear install --onlyreqdeps File_Marc-1.1.2",
    creates => "$pear_path/File/MARC",
  } ->
  exec { "pear-channel-discover":
    command => "/usr/bin/pear channel-discover pear.horde.org",
    creates => "$pear_path/Horde/Yaml",
  } ->
  exec { "pear-channel-update":
    command => "/usr/bin/pear channel-update pear.horde.org",
    creates => "$pear_path/Horde/Yaml",
  } ->
  exec { "pear-install-Horde/Horde_Yaml-beta":
    command => "/usr/bin/pear install Horde/Horde_Yaml-2.0.4",
    creates => "$pear_path/Horde/Yaml",
  } ->
  exec { "pear-install-XML_Parser-1.3.4":
    command => "pear install --force --onlyreqdeps XML_Parser-1.3.4",
    unless  => 'pear list | grep -c XML_Parser[[:blank:]]*1.3.4',
    path    => '/usr/bin/'
  }


  #### Java

  include 'java'


  #### Smarty Files

  staging::deploy { "Smarty-2.6.26.tar.gz":
    source  => "http://www.smarty.net/files/Smarty-2.6.26.tar.gz",
    target  => "/opt/staging/nli_vufind1",
    creates => "/opt/staging/nli_vufind1/Smarty-2.6.26",
  } 

  file { 'smarty-directory':
    path     => "$pear_path/Smarty",
    ensure   => 'directory',
    recurse  => true,
    mode     => "0777",
    selrange => "s0",
    selrole  => "object_r",
    seluser  => "unconfined_u",
    seltype  => "usr_t",
    source   => "/opt/staging/nli_vufind1/Smarty-2.6.26/libs",
    require  => Staging::Deploy["Smarty-2.6.26.tar.gz"],
  }


  #### Create a directory for Solr to write its logs to

  file { 'solr-logs-directory':
    path    => "$vufind_code_path/solr/jetty/logs",
    ensure  => "directory",
    mode    => "0777",
    require => Vcsrepo["$vufind_code_path"],
  }


  #### Solr java_opts - if set change the options

  if $solr_java_opts != undef {
    augeas { 'set_solr_java_opts':
      lens    => 'Properties.lns',
      incl    => "$vufind_code_path/vufind.sh",
      changes => "set JAVA_OPTIONS[1] '\"$solr_java_opts\"'",
      require => Vcsrepo["$vufind_code_path"],
      notify  => Service['vufind'],
    }
  }


  #### Apache Vhost and dependencies

  class { 'apache':
    default_mods        => false,
    default_confd_files => false,
    mpm_module          => 'prefork', # prefork needed for php
    require             => Package[$php_packages],
  }

  apache::vhost{ $vufind_url_server:
    default_vhost  => true,
    manage_docroot => false,
    docroot        => "$vufind_code_path/web",
    directories    => {},
    servername     => $vufind_url_server,
  }


  # enable mod_headers
  apache::mod { 'headers': }

  # enable mime type checking
  apache::mod { 'mime_magic': }
  apache::custom_config { 'mime-types':
    content => 'TypesConfig /etc/mime.types',
  }

  # enable apache auth modules for VuFind admin area
  apache::mod { 'auth_basic': }
  apache::mod { 'authn_core': }
  apache::mod { 'authn_file': }
  apache::mod { 'authz_user': }

  # enable apache module for reading of httpd_vufind.conf
  apache::mod { 'rewrite': }

  #mod_php
  # mod_mime and mod_dir are decalred by ::apache::mod::php
  # If removing ::apache::mod::php, re-enable them as follows:
  # apache::mod { 'mime': }
  # apache::mod { 'dir': }
  class {'::apache::mod::php':
    path => "${::apache::params::lib_path}/libphp5.so",
  }

  # enable mod_deflate
  class { '::apache::mod::deflate':
    types => [ 'text/html',
               'text/plain',
               'text/xml',
               'text/css',
               'application/json',
               'application/xml',
               'application/x-javascript',
               'application/javascript',
               'application/ecmascript',
               'application/rss-xml',
               'application/font-woff',
             ],
  }

  #### enable proxy pass for catalogue only

  if $enable_mod_proxy {
    apache::mod { 'proxy': }
    apache::mod { 'proxy_http': }
  }


  #### Security permissions need to be set up so Apache can access the VuFind code.

  file {'security_permissions_apache_vufind':
    path     => "$vufind_code_path/web",
    ensure   => "directory",
    recurse  => true,
    selrange => "s0",
    selrole  => "object_r",
    seluser  => "unconfined_u",
    seltype  => "httpd_sys_content_t",
    require  => Class['apache'],
  }

  selboolean { 'httpd_can_network_relay':
    persistent => true,
    value      => 'on',
    require    => File['security_permissions_apache_vufind'],
  }

  selboolean { 'httpd_can_sendmail':
    persistent => true,
    value      => 'on',
    require    => File['security_permissions_apache_vufind'],
  }

  selboolean { 'httpd_can_network_connect_db':
    persistent => true,
    value      => 'on',
    require    => File['security_permissions_apache_vufind'],
  }


  ##### Make directories writeable so VuFind can generate cache files

  $cache_files = ["$vufind_code_path/web/interface/compile", "$vufind_code_path/web/interface/cache", "$vufind_code_path/web/images/covers"]

  file { $cache_files:
    ensure   => "directory",
    recurse  => true,
    mode     => "0777",
    owner    => 'apache',
    group    => 'apache',
    selrange => "s0",
    selrole  => "object_r",
    seluser  => "unconfined_u",
    seltype  => "httpd_sys_rw_content_t",
    require  => File['security_permissions_apache_vufind'],
  }


  # Link VuFind to Apache

  file { 'vufind_configuration':
    path     => "$vufind_code_path/httpd-vufind.conf",
    ensure   => 'file',
    selrange => "s0",
    selrole  => "object_r",
    seluser  => "system_u",
    seltype  => "httpd_config_t",
    require  => File[$cache_files],
  }

  file { 'apache_conf_file':
    path    => '/etc/httpd/conf.d/httpd-vufind.conf',
    ensure  => link,
    target  => "$vufind_code_path/httpd-vufind.conf",
    require => [File['vufind_configuration'], Class['apache']],
    notify  => Service["httpd"],
  }


  #### Make sure that the vufind.sh and import-marc.sh are executable

  file { "$vufind_code_path/vufind.sh":
    ensure  => 'present',
    mode    => "0775",
    require => Vcsrepo["$vufind_code_path"],
  }

  file { "$vufind_code_path/import-marc.sh":
    ensure  => 'present',
    mode    => "0775",
    require => Vcsrepo["$vufind_code_path"],
  }

  file { "$vufind_code_path/import-marc-auth.sh":
    ensure  => 'present',
    mode    => "0775",
    require => Vcsrepo["$vufind_code_path"],
  }

  file { "$vufind_code_path/index-alphabetic-browse.sh":
    ensure  => 'present',
    mode    => "0775",
    require => Vcsrepo["$vufind_code_path"],
  }


  #### Make vufind service

  file {"/etc/init.d/vufind":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
    content => template('nli_vufind1/vufind-initd.erb'),
    require => [File['vufind_configuration'], File["$vufind_code_path/vufind.sh"]],
  }
  service { 'vufind':
    ensure     => running,
    enable     => true,
    provider   => init,
    hasrestart => true,
    hasstatus  => true,
    require    => File["/etc/init.d/vufind"],
  }


  #### Set Environment Variables

  file {"/etc/profile.d/vufind.sh":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => "export VUFIND_HOME=$vufind_code_path",
  }

  
  #### PHP OCI Driver for Oracle

  if $install_oci_driver {
    class {'nli_vufind1::oracle_oci':
      fileserver_mount => 'files_mount',
      require          => [Class['apache'], Package[$php_packages]],
      notify           => [Service["httpd"], Selboolean['httpd_can_network_relay']],
    }
  }


  #### Alternative PHP Cache

  if $enable_apc {
    class {'nli_vufind1::apc':
      require => [Class['apache'], Package[$php_packages], Exec["pear-install-XML_Parser-1.3.4"]],
      notify  => Service["httpd"],
    }
  }
}