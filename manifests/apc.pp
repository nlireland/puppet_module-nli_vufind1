class nli_vufind1::apc (
  
  ) inherits nli_vufind1::params {

  $yum_packages = ['httpd-devel', 'pcre-devel']

  package {$yum_packages:
    ensure   => installed,
    provider => 'yum',
  }

  exec {'pecl_install_apc':
    command => "yes '' | pecl install apc",
    unless  => 'pecl list | grep APC',
    path    => '/usr/bin/',
    require => Package[$yum_packages],
  }

  file {"apc.ini":
    path    => '/etc/php.d/apc.ini',
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => "extension=apc.so",
  }
}