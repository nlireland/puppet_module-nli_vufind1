class nli_vufind1::oracle_oci (
  $fileserver_mount = $nli_vufind1::params::fileserver_mount
  ) inherits nli_vufind1::params {

    # Directory to store oracle code

    $oracle_code_path = '/opt/oracle'

    file { 'oracle_folder':
      path   => "$oracle_code_path",
      mode   => '0775',
      ensure => 'directory',
    }


    # Install required packages

    $yum_packages = ['unzip', 'prelink', 'gcc']

    package {$yum_packages:
      ensure   => installed,
      provider => 'yum',
    }


    # Unzip files for oracle instant client

    staging::deploy { "basic.zip":
      source  => "puppet:///$fileserver_mount/basic.zip",
      target  => "$oracle_code_path",
      creates => "$oracle_code_path/instantclient_11_1",
      require => [File['oracle_folder'], Package['unzip']],
    }

    staging::deploy { "sdk.zip":
      source  => "puppet:///$fileserver_mount/sdk.zip",
      target  => "$oracle_code_path",
      creates => "$oracle_code_path/instantclient_11_1/sdk",
      require => Staging::Deploy["basic.zip"],
    }


    # Symbolic links for instant client

    file { 'link_instantclient':
      path    => "$oracle_code_path/instantclient",
      ensure  => link,
      target  => "$oracle_code_path/instantclient_11_1",
      require => [Staging::Deploy["basic.zip"], Staging::Deploy["sdk.zip"]],
    }

    file { 'link_libclntsh.so':
      path    => "$oracle_code_path/instantclient/libclntsh.so",
      ensure  => link,
      target  => "$oracle_code_path/instantclient/libclntsh.so.11.1",
      require => File['link_instantclient'],
    }

    file { 'link_libocci.so':
      path    => "$oracle_code_path/instantclient/libocci.so",
      ensure  => link,
      target  => "$oracle_code_path/instantclient/libocci.so.11.1",
      require => File['link_instantclient'],
    }

    file { 'libociei.so':
      path    => "$oracle_code_path/instantclient/libociei.so",
      seltype => "textrel_shlib_t",
      require => File['link_instantclient'],
    }

    file { 'libnnz11.so':
      path    => "$oracle_code_path/instantclient/libnnz11.so",
      seltype => "textrel_shlib_t",
      require => File['link_instantclient'],
    }

    file { 'libocijdbc11.so':
      path    => "$oracle_code_path/instantclient/libocijdbc11.so",
      seltype => "textrel_shlib_t",
      require => File['link_instantclient'],
    }

    file { 'libclntsh.so.11.1':
      path    => "$oracle_code_path/instantclient/libclntsh.so.11.1",
      seltype => "textrel_shlib_t",
      require => File['link_instantclient'],
    }

    file { 'libocci.so.11.1':
      path    => "$oracle_code_path/instantclient/libocci.so.11.1",
      seltype => "textrel_shlib_t",
      require => File['link_instantclient'],
    }


    # Security permissions for instant client

    exec {'execstack_instantclient':
      command => "execstack -c $oracle_code_path/instantclient/*.so.*",
      onlyif  => "execstack -q $oracle_code_path/instantclient/*.so.* | grep -c X[[:blank:]]$oracle_code_path/instantclient/",
      require => [Package['prelink'], File['link_libclntsh.so'], File['link_libocci.so'], File['libociei.so'], File['libnnz11.so'], File['libocijdbc11.so'], File['libclntsh.so.11.1'], File['libocci.so.11.1']],
      path    => '/usr/bin/'
    }

    selboolean {'httpd_execmem':
      persistent => true,
      value      => 'on',
      require    => Exec['execstack_instantclient'],
    }


    # Add instant client to system dynamic library loader

    file {'system_dynamic_library_loader':
      path    => '/etc/ld.so.conf.d/oracle-instantclient',
      ensure  => present,
      content => "$oracle_code_path/instantclient",
      require => Selboolean['httpd_execmem'],
    } 


    # Install OCI8 pecl library and set the correct permissions

    exec {'pecl_install_oci8':
      command => "yes instantclient,/opt/oracle/instantclient | pecl install oci8-2.0.8",
      unless  => 'pecl list | grep -c oci8',
      path    => '/usr/bin/',
      require => File['system_dynamic_library_loader'],
    }

    file { 'oci8_permissions_and_extensions':
      path     => '/usr/lib64/php/modules/oci8.so',
      mode     => "0775",
      selrange => "s0",
      selrole  => "object_r",
      seluser  => "system_u",
      seltype  => "textrel_shlib_t",
      require  => Exec['pecl_install_oci8'],
    }

    file {'oci8.ini':
      path    => '/etc/php.d/oci8.ini',
      content => 'extension=oci8.so',
      require => File['oci8_permissions_and_extensions'],
    }
}